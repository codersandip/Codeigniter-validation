<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="assets/bootstrap.css">
	<title>Validation Demo</title>
</head>
<body>
	<div class="container mt-5">
		<div class="row">
			<div class="col-md-6 offset-md-3 mt-4">
					<div class="card">
	  					<div class="card-header">Framework Form Validation Demo</div>
						  <div class="card-body">
						    <!-- <h4 class="card-title">Primary card title</h4> -->
						    <form class="form-group" method="post">
						    	<label>Name :-</label>
						    	<input type="text" name="name" placeholder="Name" class="form-control">
						    	<?= form_error('name') ?>

						    	<label>Email :-</label>
						    	<input type="text" name="email" placeholder="Email" class="form-control">
						    	<?= form_error('email') ?>

						    	<label>Mobile No. :- :-</label>
						    	<input type="text" name="mobile" placeholder="Mobile No. :-" class="form-control">
						    	<?= form_error('mobile') ?>
						    	<div class="text-center">
						    		<input type="submit" name="submit" class="btn btn-outline-primary mt-1">
						    	</div>
						    	<div>
						    		<a href="../php Validation/">Back</a>
						    	</div>
						    </form>
						  </div>
					</div>
			</div>
		</div>
	</div>
</body>
</html>
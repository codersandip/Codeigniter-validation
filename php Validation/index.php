<?php 
		$error = [];
	if (isset($_POST['submit'])) {
		// echo "<pre>";
		print_r($_POST);
		// echo "</pre>";
		if (empty($_POST['name'])) {
			$error['name'] = "The Field is required!";
		} elseif (!ctype_alpha($_POST['name'])) {
			$error['name'] = "The Field is only alpha Character!";
		}

		if (empty($_POST['email'])) {
			$error['email'] = "The Field is required!";
		} elseif (!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)) {
			$error['email'] = "The Field is Valid Email!";
		}


		if (empty($_POST['mobile'])) {
			$error['mobile'] = "The Field is required!";
		} elseif (!ctype_digit($_POST['mobile'])) {
			$error['mobile'] = "The Field is only Number!";
		}

	}
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Form Validation Demo</title>
	<link rel="stylesheet" type="text/css" href="../CIexDemo/assets/bootstrap.css">
</head>
<body>
	<div class="container mt-5">
		<div class="row">
			<div class="col-md-6 offset-md-3 mt-4">
					<div class="card">
	  					<div class="card-header">PHP Form Validation Demo</div>
						  <div class="card-body">
						    <!-- <h4 class="card-title">Primary card title</h4> -->
						    <form class="form-group" method="post">
						    	<label>Name :-</label>
						    	<input type="text" name="name" placeholder="Name" class="form-control">
						    	<div class="text-danger">*<?= isset($error['name'])?$error['name']:''; ?></div>

						    	<label>Email :-</label>
						    	<input type="text" name="email" placeholder="Email" class="form-control">
						    	<div class="text-danger">*<?= isset($error['email'])?$error['email']:''; ?></div>
						    	

						    	<label>Mobile No. :- :-</label>
						    	<input type="text" name="mobile" placeholder="Mobile No. :-" class="form-control">
						    	<div class="text-danger">*<?= isset($error['mobile'])?$error['mobile']:''; ?></div>
						    	
						    	<div class="text-center">
						    		<input type="submit" name="submit" class="btn btn-outline-primary mt-1">
						    	</div>
						    	<div>
						    		<a href="../CIexDemo/">Back</a>
						    	</div>
						    </form>
						  </div>
					</div>
			</div>
		</div>
	</div>
</body>
</html>